import 'package:flutter/material.dart';

// definir une strategie en cas d'erreur de connection
class NetworkPopupMenu<T> extends StatefulWidget {

  final T? initialValue;
  final Future<List<T>> Function() fecthData;
  final void Function(T)? onSelected;
  final Widget Function(T) itemBuilder;
  final Widget Function(T)? selectedItemBuilder;
  final Widget? placeholder;
  final String? tooltip;
  final EdgeInsetsGeometry padding;

  const NetworkPopupMenu({
    Key? key,
    this.initialValue,
    this.selectedItemBuilder,
    required this.fecthData,
    this.onSelected,
    required this.itemBuilder,
    this.placeholder,
    this.tooltip,
    this.padding = const EdgeInsets.all(8.0)
  }): super(key: key);

  @override
  _NetworkPopupMenuState<T> createState() => _NetworkPopupMenuState();
}

class _NetworkPopupMenuState<T> extends State<NetworkPopupMenu<T>> {

  late Future<List<T>> _future;
  T? _selectedItem;

  @override
  void initState() {
    super.initState();
    _future = widget.fecthData();
    _selectedItem = widget.initialValue;
  }

  void _onSelectedItem(T value) {
    setState(() { _selectedItem = value; });
    widget.onSelected?.call(value);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<T>>(
      future: _future,
      builder: (_, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError)
            return const Icon(Icons.warning_outlined, color: Colors.red);

          List<T> items = snapshot.data!;

          return PopupMenuButton<T>(
            tooltip: widget.tooltip,
            onSelected: _onSelectedItem,
            initialValue: widget.initialValue,
            padding: widget.padding,
            child: widget.selectedItemBuilder == null || _selectedItem == null
              ? widget.placeholder
              : widget.selectedItemBuilder!(_selectedItem!),
            itemBuilder: (_) => List.generate(items.length, (index) {
              var value = items[index];
              return PopupMenuItem(
                value: value,
                child: widget.itemBuilder(value)
              );
            })
          );
        }

        return const Center(child: CircularProgressIndicator());
      },
    );
  }
}
