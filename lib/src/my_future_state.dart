
enum MyFutureState {
  waiting,
  success,
  error;

  bool get isSuccess => this == MyFutureState.success;
  bool get isError => this == MyFutureState.error;
  bool get inWaiting => this == MyFutureState.waiting;
}
