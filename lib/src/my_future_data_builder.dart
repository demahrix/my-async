import 'package:flutter/material.dart';
import 'my_future_data.dart';

class MyFutureDataBuilder<T> extends StatelessWidget {

  final MyFutureData<T>? data;
  final Widget Function(BuildContext) onLoading;
  final Widget Function(BuildContext, T) onData;
  final Widget Function(BuildContext, Object?) onError;

  const MyFutureDataBuilder({
    Key? key,
    this.data,
    required this.onLoading,
    required this.onData,
    required this.onError
  }): super(key: key);

  @override
  Widget build(BuildContext context) {
    if (data == null || (data!.data == null && data!.error == null))
      return onLoading(context);
    if (data!.data != null)
      return onData(context, data!.data!);
    return onError(context, data!.error);
  }
}
